function lineChart(){
var myChart = echarts.init(chartDom);
                var option;

                option = {

                    title: {
                        left: 'center',
                        text: 'Exchange Trade Volume (USD)',
                        subtext: 'The total USD value of trading volume on major bitcoin exchanges.'
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            animation: false,
                            type: 'cross',
                            lineStyle: {
                                color: '#e63946',
                                width: 1,
                                opacity: .8
                            }
                        }
                    },
                    legend: {
                        data: []
                    },
                    toolbox: {
                        feature: {
                            dataZoom: {
                                yAxisIndex: 'none'
                            },
                            restore: {},
                            saveAsImage: {}
                        }
                    },
                    dataZoom: [{
                        show: true,
                        realtime: true,
                        start: 100,
                        end: 80
                    }, {
                        type: 'slider',
                        realtime: true,
                        start: 100,
                        end: 80
                    }],
                    grid: {
                        left: '4%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: [{
                        type: 'category',
                        data: x,
                        name: '',
                    }],
                    yAxis: {
                        type: 'value',
                        name: 'Trade Volume ($)',
                        nameLocation: 'middle',
                        nameGap: 30

                    },
                    series: [{
                        name: 'Hash Rate (TH/s)',
                        data: y,
                        type: 'line',
                        showSymbol: false,
                        itemStyle: {
                            color: '#e63946'
                        },

                        lineStyle: {
                            color: '#e63946',
                            width: 2.5
                        },

                    }]
                };

                return option && myChart.setOption(option);
            };

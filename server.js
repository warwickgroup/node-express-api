// Create express app
const express = require("express")
const app = express()
const db = require("./database.js")
const z = require("zebras")
const path = require('path');
// Server port
const HTTP_PORT = 8000 

app.use(express.static('public'));
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});

// sendFile will go here
// sendFile will go here
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '/index.html'));
  });

  app.get('/dashboard', function(req, res) {
    res.sendFile(path.join(__dirname, '/echart-test.html'));
  });

  
// Root endpoint
// app.get("/", (req, res, next) => {
//     res.json({"message":"Ok"})
// });
// // Insert here other API endpoints
// app.get("/api/LonRes/:p", (req, res, next) => {
//   var sql = "select [FULL ADDRESS] , property_type, post_code, post_out, latitude, longitude, bed_count,beds, FURNISHED, floor_area, tenure_description, start_date, final_date, initial_asking_price,  final_asking_price, days, WIG_hood  from LonResLet where post_out  = ?"
//   var params = [req.params.p]
//   db.all(sql, params, (err, rows) => {
//       if (err) {
//         res.status(400).json({"error":err.message});
//         return;
//       }
//       res.json({
//           "message":"success",
//           "data":rows
//       })
//     });
// });

// Insert here other API endpoints
app.get("/api/oneline", (req, res, next) => {
    var sql = "SELECT name, symbol, category_tags, volume_24h, price, price_24h_change, price_7d_change, market_cap,  market_cap_fully_diluted, tvl, tvl_7d_change,  revenue_24h, revenue_24h_change, revenue_7d_change, ps, twitter_followers FROM oneline WHERE market_cap IS NOT NULL"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});

app.get("/api/oneline/:y", (req, res, next) => {
    var sql = "select * from oneline where symbol = ?"
    var params = [req.params.y]
    db.get(sql, params, (err, row) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":row
        })
      });
});

// Insert here other API endpoints
app.get("/api/blockchain", (req, res, next) => {
    var sql = "select * from blockchain"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});

// Insert here other API endpoints
app.get("/api/dapp", (req, res, next) => {
    var sql = "select * from dapps"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});

// Default response for any other request
app.use(function(req, res){
    res.status(404);
});
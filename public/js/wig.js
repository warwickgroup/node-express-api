function formatDate(d) {
    var myDate = new Date(d * 1000).toLocaleDateString('en-GB', {
        year: 'numeric',
        month: 'short',
        day: 'numeric',

    });

    return myDate;

};